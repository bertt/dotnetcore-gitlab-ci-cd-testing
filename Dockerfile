FROM mcr.microsoft.com/dotnet/sdk:5.0 as build

WORKDIR /src
COPY dotnetcore-gitlab-ci-cd-testing.csproj .
RUN dotnet restore

COPY . .
RUN dotnet publish -c Release -o /out

FROM mcr.microsoft.com/dotnet/sdk:5.0 

WORKDIR /app
COPY --from=build /out/ /app

ENTRYPOINT ["dotnet", "/app/dotnetcore-gitlab-ci-cd-testing.dll"]
